﻿using Assets.Scripts.Components;
using Unity.Entities;
using Unity.Transforms;

namespace Assets.Scripts.Systems
{
    public class SpawnSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref Spawner spawner, in Translation translation, in Rotation rotation) =>
            {
                if (!EntityManager.Exists(spawner.SpawnObject))
                {
                    spawner.SpawnObject = EntityManager.Instantiate(spawner.SpawnPrefab);
                    EntityManager.SetComponentData(spawner.SpawnObject, translation);
                    EntityManager.SetComponentData(spawner.SpawnObject, rotation);
                }

            }).WithStructuralChanges().Run();
        }
    }
}
