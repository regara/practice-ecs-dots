﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

public class EnemySystem : SystemBase
{
    private Unity.Mathematics.Random rng = new Unity.Mathematics.Random(1234);

    protected override void OnUpdate()
    {
        var raycaster = new MovementRayCast { physicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>().PhysicsWorld };
        rng.NextInt();
        var rngTemp = rng;


        Entities.ForEach((ref Moveable moveable, ref Enemy enemy, in Translation translation) =>
        {

            bool hitWall = raycaster.CheckRay(translation.Value, moveable.Direction, moveable.Direction);

            if (math.distance(translation.Value, enemy.previousCell) > 0.9f || hitWall)
            {
                enemy.previousCell = math.round(translation.Value);

                var validDir = new NativeList<float3>(Allocator.Temp);

                if (!raycaster.CheckRay(translation.Value, new float3(0, 0, -1), moveable.Direction))
                {
                    validDir.Add(new float3(0, 0, -1));
                }

                if (!raycaster.CheckRay(translation.Value, new float3(0, 0, 1), moveable.Direction))
                {
                    validDir.Add(new float3(0, 0, 1));
                }

                if (!raycaster.CheckRay(translation.Value, new float3(-1, 0, 0), moveable.Direction))
                {
                    validDir.Add(new float3(-1, 0, 0));
                }

                if (!raycaster.CheckRay(translation.Value, new float3(1, 0, 0), moveable.Direction))
                {
                    validDir.Add(new float3(1, 0, 0));
                }

                if (validDir.Length > 0)
                    moveable.Direction = validDir[rngTemp.NextInt(validDir.Length)];

                validDir.Dispose();
            }

        }).Schedule();

        CompleteDependency();
    }

    private struct MovementRayCast
    {
        public PhysicsWorld physicsWorld;
        public bool CheckRay(float3 position, float3 direction, float3 currentDirection)
        {

            if (direction.Equals(-currentDirection))
            {
                return true;
            }

            var ray = new RaycastInput()
            {
                Start = position,
                End = position + (direction * 0.7f),
                Filter = new CollisionFilter
                {
                    GroupIndex = 0,
                    BelongsTo = 1u << 1,
                    CollidesWith = 1u << 2
                }
            };

            return physicsWorld.CastRay(ray);
        }
    }
}