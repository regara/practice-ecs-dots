using Unity.Entities;
using Unity.Physics;

public class MoveableSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref PhysicsVelocity physicsVelocity, in Moveable moveable) =>
        {
            var step = moveable.Direction * moveable.Speed;


            physicsVelocity.Linear = step;
        }).Schedule();
    }
}
