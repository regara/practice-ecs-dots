using Assets.Scripts.Components;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class PlayerSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");
        var deltaTime = Time.DeltaTime;

        Entities
            .WithAll<Player>()
            .ForEach((ref Moveable moveable) =>
        {
            moveable.Direction = new float3(x, 0, y);
        }).Schedule();

        var entityCommandBuffer =
            World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>().CreateCommandBuffer();

        Entities
            .WithAll<Player>()
            .ForEach((Entity entity, ref Health health, ref PowerPill powerPill, ref Damage damage) =>
            {
                damage.Value = 100;
                powerPill.pillTimmer -= deltaTime;
                health.InvincibleTimer = powerPill.pillTimmer;

                if (powerPill.pillTimmer <= 0)
                {
                    entityCommandBuffer.RemoveComponent<PowerPill>(entity);
                    damage.Value = 0;
                }

            }).Schedule();
    }
}
