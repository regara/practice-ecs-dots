﻿using Assets.Scripts.Components;
using Unity.Entities;

public class DamageSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var deltaTime = Time.DeltaTime;

        Entities.ForEach((DynamicBuffer<CollisionBuffer> CollisionBuffer, ref Health health) =>
        {
            for (int i = 0; i < CollisionBuffer.Length; i++)
            {
                if (health.InvincibleTimer <= 0 && HasComponent<Damage>(CollisionBuffer[i].Entity))
                {
                    health.Value -= GetComponent<Damage>(CollisionBuffer[i].Entity).Value;
                    health.InvincibleTimer = 1;
                }
            }
        }).Schedule();

        Entities
            .WithNone<Kill>()
            .ForEach((Entity entity, ref Health health) =>
        {
            health.InvincibleTimer -= deltaTime;

            if (health.Value <= 0)
            {
                EntityManager.AddComponentData(entity, new Kill {Timer = health.KillTimer});
            }

        }).WithStructuralChanges().Run();

        var entityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        var entityCommandBuffer = entityCommandBufferSystem.CreateCommandBuffer();

        Entities.ForEach((Entity entity, int entityInQueryIndex, ref Kill kill) =>
        {
            kill.Timer -= deltaTime;

            if (kill.Timer <= 0)
            {
                entityCommandBuffer.DestroyEntity(entity);
            }
        }).Schedule();

        entityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}