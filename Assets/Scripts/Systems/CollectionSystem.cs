﻿using System;
using Assets.Scripts.Components;
using Unity.Entities;

namespace Assets.Scripts.Systems
{
    public class CollectionSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var entityCommandBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>().CreateCommandBuffer();

            Entities
                .WithAll<Player>()
                .ForEach((Entity playerEntity, DynamicBuffer<TriggerBuffer> triggerBuffer) =>
                {
                    for (int i = 0; i < triggerBuffer.Length; i++)
                    {
                        var entity = triggerBuffer[i].Entity;

                        if (HasComponent<Collectable>(entity) && !HasComponent<Kill>(entity))
                        {
                            entityCommandBuffer.AddComponent(entity, new Kill { Timer = 0 });
                            GameManager.Instance.AddPoints(GetComponent<Collectable>(entity).points);
                        }

                        if (HasComponent<PowerPill>(entity) && !HasComponent<Kill>(entity))
                        {
                            entityCommandBuffer.AddComponent(playerEntity, GetComponent<PowerPill>(entity));
                            entityCommandBuffer.AddComponent(entity, new Kill { Timer = 0 });
                        }
                    }
                }).WithoutBurst().Run();
        }
    }
}
