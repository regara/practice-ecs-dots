using Assets.Scripts.Components;
using Unity.Entities;
using UnityEngine;

[AlwaysUpdateSystem]
public class GameStateSystem : SystemBase
{
    protected override void OnUpdate()
    {
        // GetEntityQuery(typeof(Pellet));
        // More optimal to do readonly if were only reading
        var pelletQuery = GetEntityQuery(ComponentType.ReadOnly<Pellet>());
        var playerQuery = GetEntityQuery(ComponentType.ReadOnly<Player>());

        if (pelletQuery.CalculateEntityCount() <= 0)
            GameManager.Instance.Win();

        if (playerQuery.CalculateEntityCount() <= 0)
            GameManager.Instance.Lose();
    }
}
