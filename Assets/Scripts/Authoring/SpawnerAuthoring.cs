using System.Collections.Generic;
using Assets.Scripts.Components;
using Unity.Entities;
using UnityEngine;

[DisallowMultipleComponent]
public class SpawnerAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
{
    public GameObject SpawnObject;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new Spawner { SpawnPrefab = conversionSystem.GetPrimaryEntity(SpawnObject) });
    }

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
    {
        referencedPrefabs.Add(SpawnObject);
    }
}
