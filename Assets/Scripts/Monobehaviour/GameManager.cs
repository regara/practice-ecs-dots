using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public int Score;
    public TextMeshProUGUI PelletsUi, ScoreTextUi;
    public GameObject TitleUi, GameUi, WinUi, LoseUi;

    public void Awake()
    {
        Instance = this;
    }
    public void Reset()
    {
        SwitchUi(TitleUi);
        Score = 0;
    }

    public void InGame()
    {
        SwitchUi(gameObject);
    }

    public void Win()
    {
        SwitchUi(WinUi);
    }

    public void Lose()
    {
        SwitchUi(LoseUi);
    }

    public void SwitchUi(GameObject newUi)
    {
        TitleUi.SetActive(false);
        GameUi.SetActive(false);
        WinUi.SetActive(false);
        LoseUi.SetActive(false);

        newUi.SetActive(true);
    }

    public void AddPoints(int points)
    {
        Score += points;
        ScoreTextUi.text = $"Score : {Score}";
    }
}
