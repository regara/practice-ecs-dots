using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct Moveable : IComponentData
{
    public float Speed;
    public float3 Direction;
}
