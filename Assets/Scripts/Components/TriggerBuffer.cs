﻿using Unity.Entities;

namespace Assets.Scripts.Components
{
    [GenerateAuthoringComponent]
    public struct TriggerBuffer : IBufferElementData
    {
        public Entity Entity;
    }
}