﻿using Unity.Entities;

namespace Assets.Scripts.Components
{
    public struct CollisionBuffer : IBufferElementData
    {
        public Entity Entity;
    }
}
