﻿using Unity.Entities;

namespace Assets.Scripts.Components
{
    [GenerateAuthoringComponent]
    public struct Collectable : IComponentData
    {
        public int points;
    }
}